package com.example.planit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ZipCodeActivity extends AppCompatActivity {

    public static final String EXTRA_NUM = "com.example.planit.EXTRA_NUM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zip_code);


        Button button = (Button) findViewById(R.id.zipButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWeather();
            }
        });
    }

    public void openWeather(){
        EditText userZip = (EditText) findViewById(R.id.zipCode);
        int zipInt = Integer.parseInt(userZip.getText().toString());

        Intent intent = new Intent(this, WeatherActivity.class);
        intent.putExtra(EXTRA_NUM, zipInt);
        startActivity(intent);
    }

}
